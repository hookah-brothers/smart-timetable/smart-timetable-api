import { AggregateRoot } from '@libs/ddd/domain/base-classes';
import { ID } from '@libs/ddd/domain/value-objects/id.value-object';

export type OrmEntityPropsPort<OrmEntity> = Omit<
  OrmEntity,
  'id' | 'createdAt' | 'updatedAt' | 'deletedAt' | 'properties'
>;

export interface EntityPropsPort<EntityProps> {
  id: ID;
  props: EntityProps;
}

export interface OrmMapperPort<
  Entity extends AggregateRoot<unknown>,
  OrmEntity,
> {
  toDomainEntity(ormEntity: OrmEntity, trxId?: unknown): Promise<Entity>;

  toOrmEntity(entity: Entity): Promise<OrmEntity>;

  toDomainEntities(ormEntity: OrmEntity, trxId?: unknown): Promise<Entity[]>;

  toOrmEntities(entity: Entity): Promise<OrmEntity[]>;
}
