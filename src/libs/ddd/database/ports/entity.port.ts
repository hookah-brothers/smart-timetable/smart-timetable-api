export interface EntityPort {
  id: string;
  updatedAt: Date;
  createdAt: Date;
  deletedAt?: Date;
}
