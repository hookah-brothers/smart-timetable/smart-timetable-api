import { BaseEntityProps } from '@libs/ddd/domain/base-classes';
import { ID } from '@libs/ddd/domain/value-objects/id.value-object';
import { UUID } from '@libs/ddd/domain/value-objects/uuid.value-object';
import { DeepPartial } from '@libs/types';

export type QueryParams<EntityProps> = DeepPartial<
  BaseEntityProps & EntityProps
>;

export interface Save<Entity> {
  save(entity: Entity): Promise<UUID>;
}

export interface SaveMultiple<Entity> {
  saveMultiple(entities: Entity[]): Promise<UUID[]>;
}

export interface FindOne<Entity, EntityProps> {
  findOne(params?: QueryParams<EntityProps>): Promise<Entity | undefined>;
}

export interface FindOneById<Entity> {
  findOneById(id: ID | string): Promise<Entity | undefined>;
}

export interface FindMany<Entity, EntityProps> {
  findMany(params?: QueryParams<EntityProps>): Promise<Entity[]>;
}

export interface FindOneOrFail<Entity, EntityProps> {
  findOne(params?: QueryParams<EntityProps>): Promise<Entity>;
}

export interface FindOneByIdOrFail<Entity> {
  findOneById(id: ID | string): Promise<Entity>;
}

export interface DeleteOne<Entity> {
  delete(entity: Entity): Promise<UUID>;
}

export interface RepositoryPort<Entity, EntityProps>
  extends Save<Entity>,
    SaveMultiple<Entity>,
    FindOne<Entity, EntityProps>,
    FindOneOrFail<Entity, EntityProps>,
    FindOneById<Entity>,
    FindOneByIdOrFail<Entity>,
    FindMany<Entity, EntityProps>,
    DeleteOne<Entity> {}
