import { AggregateRoot } from '@libs/ddd/domain/base-classes';
import { DeepPartial } from '@libs/types';
import { OrmMapperPort } from '../ports/orm-mapper.port';
import { QueryParams } from '../ports/repository.ports';

export abstract class RepositoryBase<
  Entity extends AggregateRoot<unknown>,
  EntityProps,
  OrmEntity,
> {
  protected constructor(
    protected readonly mapper: OrmMapperPort<Entity, OrmEntity>,
  ) {}

  protected abstract prepareQuery(
    params: QueryParams<EntityProps>,
  ): DeepPartial<OrmEntity>;
}
