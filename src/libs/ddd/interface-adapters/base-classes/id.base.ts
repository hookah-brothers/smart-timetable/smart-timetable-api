import { Id } from '../interfaces/id.interface';

export class IdBase implements Id {
  constructor(id: string) {
    this.id = id;
  }

  id: string;
}
