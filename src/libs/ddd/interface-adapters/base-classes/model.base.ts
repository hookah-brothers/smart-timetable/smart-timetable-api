import { EntityPort } from '@libs/ddd/database/ports/entity.port';
import { IdBase } from './id.base';

export class ModelBase extends IdBase {
  constructor(entity: EntityPort) {
    super(entity.id);

    this.createdAt = entity.createdAt.toISOString();
    this.updatedAt = entity.updatedAt.toISOString();
    this.deletedAt = entity.deletedAt?.toISOString();
  }

  readonly createdAt: string;

  readonly updatedAt: string;

  readonly deletedAt?: string;
}
