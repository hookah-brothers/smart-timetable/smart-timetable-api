import { ArgumentInvalidException } from '@libs/exceptions';
import { DomainPrimitive, ValueObject } from '../base-classes';
import { Guard } from '../guard';

export class UrlVO extends ValueObject<string> {
  constructor(value: string) {
    super({ value });
    this.props.value = UrlVO.format(value);
  }

  get value(): string {
    return this.props.value;
  }

  protected validate({ value }: DomainPrimitive<string>): void {
    if (!Guard.isUrl(value)) {
      throw new ArgumentInvalidException('Url has incorrect format');
    }
  }

  static format(url: string): string {
    return url.trim();
  }
}
