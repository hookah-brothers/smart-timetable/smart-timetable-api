import { ArgumentInvalidException } from '@libs/exceptions';
import { isISO8601 } from 'class-validator';
import { DomainPrimitive, ValueObject } from '../base-classes';

export class DateISOVO extends ValueObject<string> {
  constructor(value: string) {
    super({ value });
  }

  public get value(): string {
    return this.props.value;
  }

  public static now(): DateISOVO {
    return new DateISOVO(new Date().toISOString());
  }

  protected validate({ value }: DomainPrimitive<string>): void {
    if (
      value.length !== 10 ||
      !isISO8601(value, {
        strict: true,
      })
    ) {
      throw new ArgumentInvalidException(
        'String is not in iso format ex. YYYY-MM-DD',
      );
    }
  }
}
