export type TransactionId = string;

export abstract class UnitOfWork<TransactionType = unknown> {
  abstract start(): Promise<TransactionId>;
  abstract execute(transactionId: TransactionId): Promise<void>;
  abstract commit(transactionId: TransactionId): Promise<void>;
  abstract rollback(transactionId: TransactionId): Promise<void>;
  abstract getTrx(transactionId: TransactionId): TransactionType;
}
