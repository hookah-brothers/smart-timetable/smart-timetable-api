import {
  loadConfiguration,
  validationSchema,
} from '@infrastructure/configs/environment.config';
import { ObjectionModule } from '@infrastructure/database/objection/objection.module';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';

const factories = [
  ConfigModule.forRoot({
    isGlobal: true,
    load: [loadConfiguration],
    validationSchema: validationSchema,
    validationOptions: { abortEarly: true },
  }),
  ObjectionModule,
];

@Module({
  imports: [...factories],
  controllers: [AppController],
})
export class AppModule {}
