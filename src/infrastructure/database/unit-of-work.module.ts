import { ObjectionUnitOfWorkBase } from '@libs/unit-of-work/adapters/objection-unit-of-work';
import { UnitOfWork } from '@libs/unit-of-work/unit-of-work.base';
import { Global, Module } from '@nestjs/common';

const unitOfWorkSingleton = new ObjectionUnitOfWorkBase();

const unitOfWorkSingletonProvider = {
  provide: UnitOfWork,
  useFactory: () => unitOfWorkSingleton,
};

@Global()
@Module({
  providers: [unitOfWorkSingletonProvider],
  exports: [UnitOfWork],
})
export class UnitOfWorkModule {}
