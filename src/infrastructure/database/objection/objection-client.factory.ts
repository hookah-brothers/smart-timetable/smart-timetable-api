import { FactoryProvider } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import Knex from 'knex';
import { Model } from 'objection';

const OBJECTION_CLIENT_PROVIDER_NAME = 'OBJECTION_CLIENT_PROVIDER_NAME';

export const ObjectionClientFactory: FactoryProvider = {
  provide: OBJECTION_CLIENT_PROVIDER_NAME,
  useFactory: (configService: ConfigService) => {
    const knex = Knex({
      client: 'pg',
      debug: configService.get<string>('nodeEnv') === 'development',
      connection: {
        host: configService.get<string>('database.host'),
        port: configService.get<number>('database.port'),
        user: configService.get<string>('database.user'),
        password: configService.get<string>('database.password'),
        database: configService.get<string>('database.dbName'),
      },
    });

    Model.knex(knex);
    return knex;
  },
  inject: [ConfigService],
};
