import {
  EntityPropsPort,
  OrmEntityPropsPort,
} from '@libs/ddd/database/ports/orm-mapper.port';
import {
  AggregateRoot,
  CreateEntityProps,
} from '@libs/ddd/domain/base-classes';
import { DateVO } from '@libs/ddd/domain/value-objects/date.value-object';
import {
  TransactionId,
  UnitOfWork,
} from '@libs/unit-of-work/unit-of-work.base';
import { Model } from 'objection';
import { ObjectionEntityBase } from './objection-entity.base';

export type ObjectionOrmEntityProps<OrmEntity> = Omit<
  OrmEntityPropsPort<OrmEntity>,
  keyof Model
>;

export type ObjectionEntityProps<EntityProps> = EntityPropsPort<EntityProps>;

export abstract class ObjectionOrmMapper<
  Entity extends AggregateRoot<unknown>,
  OrmEntity,
> {
  constructor(
    private entityConstructor: new (props: CreateEntityProps<any>) => Entity,
    protected readonly unitOfWork?: UnitOfWork,
  ) {}

  protected abstract toOrmProps(
    entity: Entity,
  ): Promise<ObjectionOrmEntityProps<OrmEntity>>;

  protected abstract toDomainProps(
    ormEntity: OrmEntity,
    trxId?: TransactionId,
  ): Promise<ObjectionEntityProps<unknown>>;

  async toDomainEntity(
    ormEntity: OrmEntity,
    trxId?: TransactionId,
  ): Promise<Entity> {
    const { id, props } = await this.toDomainProps(ormEntity, trxId);
    const ormEntityBase: ObjectionEntityBase =
      ormEntity as unknown as ObjectionEntityBase;
    return new this.entityConstructor({
      id,
      props,
      createdAt: new DateVO(ormEntityBase.createdAt),
      updatedAt: new DateVO(ormEntityBase.updatedAt),
      deletedAt: ormEntityBase.deletedAt
        ? new DateVO(ormEntityBase.deletedAt)
        : null,
    });
  }

  async toOrmEntity(entity: Entity): Promise<OrmEntity> {
    const props = await this.toOrmProps(entity);

    return Model.fromJson({
      ...props,
      id: entity.id.value,
      createdAt: entity.createdAt.value,
      updatedAt: entity.updatedAt.value,
      deletedAt: entity.deletedAt?.value || null,
    }) as unknown as OrmEntity;
  }
}
