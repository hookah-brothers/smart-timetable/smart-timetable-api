import { EntityPort } from '@libs/ddd/database/ports/entity.port';
import { Model } from 'objection';

export abstract class ObjectionEntityBase extends Model implements EntityPort {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
}
