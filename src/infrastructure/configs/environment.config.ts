import * as Joi from 'joi';

type EnvironmentTypes = 'development' | 'staging' | 'production';
export let currentEnvironment: EnvironmentTypes;

export const loadConfiguration = (): Record<string, unknown> => {
  currentEnvironment = process.env.APP_ENVIRONMENT as EnvironmentTypes;

  return {
    nodeEnv: process.env.NODE_ENV,
    port: process.env.PORT,
    corsClientUrls: process.env.CORS_CLIENT_URLS?.split(','),
    database: {
      host: process.env.DATABASE_HOST,
      port: parseInt(process.env.DATABASE_PORT as string),
      user: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      dbName: process.env.DATABASE_NAME,
    },
  };
};

export const validationSchema = Joi.object({
  //ENV
  NODE_ENV: Joi.string()
    .valid('development', 'staging', 'production')
    .required(),
  PORT: Joi.number().required(),
  CORS_CLIENT_URLS: Joi.string().required(),

  //DATABASE
  DATABASE_HOST: Joi.string().required(),
  DATABASE_PORT: Joi.number().required(),
  DATABASE_USER: Joi.string().required(),
  DATABASE_PASSWORD: Joi.string().required(),
  DATABASE_NAME: Joi.string().required(),
});
